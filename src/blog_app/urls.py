from django.contrib import admin
from django.urls import path, include

from .views import post_list, post_detail, PostListView

app_name = 'blog'

urlpatterns = [
    path('post/', post_list, name='post_list'),
    path('post/<int:year>/<int:month>/<int:day>/<slug:post>/', post_detail, name='post_detail'),
    path('', PostListView.as_view(), name='post_lists')
]
